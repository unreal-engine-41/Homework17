﻿#include <iostream>
#include <cmath>

/*
class Example
{
private:
	int a;
public:
	int GetA()
	{
		return a;
	}
	void SetA(int newA)
	{
		a = newA;
	}
};

int main()
{
	Example temp, temp1;
	temp.SetA(5);
	temp1.SetA(10);
	std::cout << temp.GetA() << " " << temp1.GetA();
}
*/
/*
class Example
{
private:
	int a;

public:
	Example(int newA) : a(newA) {}

	int GetA()
	{
		return a;
	}
};

int main()
{
	Example temp(11);
	std::cout << temp.GetA();
}
*/

class Vector
{
public:
	Vector() : x(2), y(5), z(3) {}

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {}

	void Show()
	{
		std::cout << x << " " << y << " " << z;
	}

	void Long()
	{
		std::cout << '\n' << sqrt(x * x + y * y + z * z);
	}

private:
	double x = 0, y = 1, z = 3;
};

int main()
{
//  Vector v;
	Vector v;
	v.Show();
	v.Long();
}